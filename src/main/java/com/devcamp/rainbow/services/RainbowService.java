package com.devcamp.rainbow.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    private String[] rainbows =  { "red", "orange", "yellow", "green", "blue", "indigo", "violet" };

    public ArrayList<String> searchRainbows(String keyword) {
        ArrayList<String> results = new ArrayList<>();

        for (String color : rainbows) {
            if(color.contains(keyword)) {
                results.add(color);
            }
        }

        return results;
    }

    public String getRainbow(int index) {
        if(index >= 0 && index <= 6) {
            return rainbows[index];
        }

        return null;
    }
}
