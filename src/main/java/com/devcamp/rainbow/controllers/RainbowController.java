package com.devcamp.rainbow.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.rainbow.services.RainbowService;

@RestController
@CrossOrigin
@RequestMapping("/")
public class RainbowController {
    @Autowired
    RainbowService rainbowService;

    @GetMapping("/rainbow-request-query")
    public ArrayList<String> searchRainbows(@RequestParam(value="filter", defaultValue="") String keyword) {
        return rainbowService.searchRainbows(keyword);
    }

    @GetMapping("/rainbow-request-param/{index}")
    public String getRainbow(@PathVariable int index) {
        return rainbowService.getRainbow(index);
    }
}
